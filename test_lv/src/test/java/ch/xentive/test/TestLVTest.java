package ch.xentive.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

/**
 * 
 * @author sebastian.levet
 */
public class TestLVTest {
	
	@Test
	public void testTestLV() {
		Assert.assertTrue(1 == TestLV.testLV(createList(1)));
		Assert.assertTrue(100 == TestLV.testLV(createList(100)));
		Assert.assertTrue(17 == TestLV.testLV(createList(17)));
		Assert.assertTrue(null == TestLV.testLV(createList(null)));
		
		// test exception222
		boolean exception = false;
		try {
			TestLV.testLV(null);
		} catch(IllegalArgumentException e) {
			exception = true;
		}
		Assert.assertTrue(exception);
	}

	/**
	 * @param missingNumber this number is not in the list
	 * @return a list of number from 1 to 100. the list is shuffled. 
	 */
	private List<Integer> createList(Integer missingNumber) {
		List<Integer> numberList = new ArrayList<Integer>();
		for(int i=1; i<101; i++) {
			numberList.add(i);
		}
		
		if(missingNumber!=null) {
			numberList.remove(missingNumber);
		}
		Collections.shuffle(numberList);
		return numberList;
	}
}