package ch.xentive.test;


import java.util.List;

/**
 * Test Levet Sébastian (15.01.2016)
 * @author sebastian.levet
 *
 */
public class TestLV {
	
	/**
	 * @param numberList a list of 99 numbers elements from 1 to 100. 
	 * @return the first missing number of the list.
	 */
	public static Integer testLV(List<Integer> numberList) {
		// check numberList
		if(numberList==null) {
			throw new IllegalArgumentException("the number list must not be null");
		}
		
		// return the first missing number
		for(Integer i=1; i<101; i++) {
			if(!numberList.contains(i)) {
				return i;
			}
		}
		// modif 2
		return null;
	}
}